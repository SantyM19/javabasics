# JavaBasic
## Practica JavaBasic | Java | SofkaU

Ejercicios del Campus

### First Part
1. Declara 2 variables numéricas (con el valor que desees), he indica cual es mayor de los dos. Si son iguales indicarlo también. Ve cambiando los valores para comprobar que funciona.
   - La solucion se puede observar en la rama 1, o accediendo a GreaterThan.java 
2. Al ejercicio anterior agregar entrada por consola de dos valores e indicar si son mayores, menores o iguales. 
   - La solucion se puede observar en la rama 1, o accediendo a GreaterThan.java 
3. Haz una aplicación que calcule el área de un círculo(pi*R2). El radio se pedirá por teclado (recuerda pasar de String a double con Double.parseDouble). Usa la constante PI y el método pow de Math. 
   - La solucion se puede observar en la rama 3, o accediendo a CircleArea.java 
4. Lee un número por teclado que pida el precio de un producto (puede tener decimales) y calcule el precio final con IVA. El IVA sera una constante que sera del 21%.
   - La solucion se puede observar en la rama 4, o accediendo a ProductPrice.java
5. Muestra los números impares y pares del 1 al 100 (ambos incluidos). Usa un bucle while.
   - La solucion se puede observar en la rama 5, o accediendo a OddAndEvenNumbers.java
6. Realiza el ejercicio anterior usando un ciclo for.
   - La solucion se puede observar en la rama 6, o accediendo a OddAndEvenNumbers.java
7. Lee un número por teclado y comprueba que este numero es mayor o igual que cero, si no lo es lo volverá a pedir (do while), después muestra ese número por consola.
   - La solucion se puede observar en la rama 7, o accediendo a GreaterThanOrEqualZero.java
8. Crea una aplicación por consola que nos pida un día de la semana y que nos diga si es un día laboral o no. Usa un switch para ello.
   - La solucion se puede observar en la rama 8, o accediendo a WeekDays.java
9. Del texto, “La sonrisa sera la mejor arma contra la tristeza” Reemplaza todas las a del String anterior por una e, adicionalmente concatenar a esta frase una adicional que ustedes quieran incluir por consola y las muestre luego unidas.
   - La solucion se puede observar en la rama 9, o accediendo a Text.java
   
### Second Part

0. Realizar una aplicación de consola, que al ingresar una frase por teclado elimine los espacios que esta contenga.
   - La solucion se puede observar en la rama 10, o accediendo a InputText.java
1. Realizar la construcción de un algoritmo que permita de acuerdo a una frase pasada por consola, indicar cual es la longitud de esta frase, adicionalmente cuantas vocales tiene de “a,e,i,o,u”.
   - La solucion se puede observar en la rama 11, o accediendo a CountVowels.java
2. Pedir dos palabras por teclado, indicar si son iguales, si no son iguales mostrar sus diferencias.
   - La solucion se puede observar en la rama 12, o accediendo a CountVowels.java
3. Realizar un algoritmo que permita consulta la fecha y hora actual en el formato (AAAA/MM/DD) (HH:MM:SS)
   - La solucion se puede observar en la rama 13, o accediendo a Time.java
4. Crear un programa que pida un numero por teclado y que imprima por pantalla los números desde el numero introducido hasta 1000 con saldos de 2 en 2.
   - La solucion se puede observar en la rama 14, o accediendo a CountToAThousand.java
5. Menu Cinematografico
   - La solucion se puede observar en la rama 15, o accediendo a CinematographicManagement.java
6. Clase llamada Persona
   - La solucion se puede observar en la rama 16, o accediendo al directorio Person
7. Super Clase llamada Electrodomestico
   - La solucion se puede observar en la rama 17, o accediendo al directorio HomeAppliance
8. Clase llamada Serie
   - La solucion se puede observar en la rama 18, o accediendo al directorio SerieAndVideoGames