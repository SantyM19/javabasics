/*
Realizar  la  construcción  de  un  algoritmo  que  permita  de  acuerdo  a  una  frase
pasada  por  consola,  indicar  cual  es  la  longitud  de  esta  frase,  adicionalmente
cuantas vocales tiene de “a,e,i,o,u”
* */

import java.util.Scanner;

public class CountVowels {

    public static int countA = 0, countE = 0, countI = 0, countO = 0, countU = 0;

    public static void main (String[] args){
        String inputText;

        Scanner read = new Scanner(System.in);
        System.out.println("Insert Your New Text");
        inputText = read.nextLine();

        processing(inputText, inputText.length());

        System.out.println("Your Text: \n" + inputText + "\n" +
                "The Text Length : " + inputText.length() + "\n" +
                "How many vowels Does this Text have? \n" +
                "a : " + countA + "\n" +
                "e : " + countE + "\n" +
                "i : " + countI + "\n" +
                "o : " + countO + "\n" +
                "u : " + countU );
    }

    static void processing (String inputText, int lengthText){
        for(int i = 0 ; i < lengthText ; i++ ){
            count(inputText.toUpperCase().charAt(i));
        }
    }

    static void count(char letter) {
        switch (letter){
            case 'A':
                countA++;
                break;
            case 'E':
                countE++;
                break;
            case 'I':
                countI++;
                break;
            case 'O':
                countO++;
                break;
            case 'U':
                countU++;
                break;
        }
    }
}
