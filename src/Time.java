import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
Realizar un algoritmo que permita consulta la fecha y hora actual en el formato
(AAAA/MM/DD) (HH:MM:SS)
* */

public class Time {
    public static void main(String[] args) {
        Date date = new Date();
        DateFormat dateHourFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        System.out.println("Date Time: " + dateHourFormat.format(date));
    }
}
