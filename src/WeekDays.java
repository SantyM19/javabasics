import java.util.Scanner;

/*
Crea una aplicación por consola que nos pida un día de la semana y que nos diga
si es un día laboral o no. Usa un switch para ello.
* */
public class WeekDays {
    public static void main(String[] args) {
        int weekday;

        Scanner read = new Scanner(System.in);

        System.out.println("Enter the WeekDay \n" +
                "1 for Monday \n" +
                "2 for Tuesday \n" +
                "3 for Wednesday \n" +
                "4 for Thursday \n" +
                "5 for Friday \n" +
                "6 for Saturday \n" +
                "7 for sunday \n");
        weekday = read.nextInt();

        System.out.println(laboralDay(weekday));

    }
    public static String laboralDay( int weekday){
        switch (weekday) {
            case 1:
                return "Today is Monday \nA WorkDay";
            case 2:
                return "Today is Tuesday \nA WorkDay";
            case 3:
                return "Today is Wednesday \nA WorkDay";
            case 4:
                return "Today is Thursday \nA WorkDay";
            case 5:
                return "Today is Friday \nA WorkDay";
            case 6:
                return "Today is Saturday \nA HoliDay";
            case 7:
                return "Today is Sunday \nA HoliDay";
            default:
                return "Wrong!";
        }
    }
}
