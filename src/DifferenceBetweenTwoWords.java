import java.util.Scanner;

/*
Pedir dos palabras por teclado, indicar si son iguales, sino son iguales mostrar sus
diferencias.
* */
public class DifferenceBetweenTwoWords {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    public static void main (String[] args){
        String inputText,inputText2;
        int lengthText, lengthText2, length;
        boolean equal;

        Scanner read = new Scanner(System.in);
        System.out.println("Insert Your Word");
        inputText = read.nextLine();

        System.out.println("Insert Your Second Word");
        inputText2 = read.nextLine();

        lengthText = inputText.length();
        lengthText2 = inputText2.length();

        length = Math.min(lengthText, lengthText2);
        equal = processingWords(length, inputText, inputText2);
        equivalent(lengthText,lengthText2, inputText2 , inputText, equal);

    }

    private static void equivalent(int lengthText, int lengthText2, String inputText, String inputText2, boolean equal) {
        if (lengthText == lengthText2 && equal){
            System.out.println(ANSI_CYAN + "These letters are Equivalent" + ANSI_CYAN);
        }else{
            textGreater(lengthText, lengthText2, inputText, inputText2);
            System.out.println(ANSI_YELLOW + "These letters are Different" + ANSI_YELLOW);
        }
    }

    private static void textGreater(int lengthText, int lengthText2, String inputText, String inputText2) {
        int lengthDifference;
        if(lengthText > lengthText2){
            lengthDifference = lengthText - lengthText2;
                FinalWords(0,lengthDifference,inputText);
        }else{
            lengthDifference = lengthText2 - lengthText;
            FinalWords(1,lengthDifference,inputText2);
        }
    }

    private static boolean processingWords(int length, String inputText, String inputText2) {
        char letter, letter2;
        boolean equal = true;
        for(int i = 0 ; i < length; i++ ){
            letter = inputText.charAt(i);
            letter2 = inputText2.charAt(i);
            equal = salida(letter, letter2);
        }
        return equal;
    }

    private static boolean salida(char letter, char letter2) {
        boolean equal;
        if (letter == letter2){
            System.out.println(ANSI_GREEN + "Letter " + letter + " == " + letter2 + " Equivalent" + ANSI_GREEN);
            return equal = true;
        }else{
            System.out.println(ANSI_RED + "Letter " + letter + " != " + letter2 + " Different " + ANSI_RED);
            return equal = false;
        }
    }

    static void FinalWords(int textNumber, int difference, String finalText) {
        int x = finalText.length() - difference;
        if (textNumber == 0){
            firstDifference(difference,finalText,x);
        }else{
            secondDifference(difference,finalText,x);
        }
    }

    static void firstDifference(int difference, String finalText, int x){
        for(int i = 0 ; i < difference ; i++){
            System.out.println(ANSI_RED + "Letter " + finalText.charAt(i + x) + " !=   Different " + ANSI_RED);
        }
    }

    static void secondDifference(int difference, String finalText, int x){
        for(int i = 0 ; i < difference ; i++){
            System.out.println(ANSI_RED + "Letter   != " + finalText.charAt(i + x) + " Different " + ANSI_RED);
        }
    }

}
