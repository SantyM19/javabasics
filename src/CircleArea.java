/*
Haz una aplicación que calcule el área de un círculo(pi*R2). El radio se pedirá por
teclado  (recuerda  pasar  de  String  a  double  con Double.parseDouble).  Usa  la
constante PI y el método pow de Math
* */

import java.util.Scanner;

public class CircleArea {
    public static void main(String[] args) {
        double radio, area;

        Scanner read = new Scanner(System.in);

        System.out.println("Enter the Radio in meters");
        radio = read.nextDouble();
        area = Math.PI * Math.pow(2,radio);

        System.out.println("The Area is " + area + " m^2");
    }
}
