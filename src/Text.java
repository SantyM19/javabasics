import java.util.Scanner;

/*
Del texto, “La sonrisa sera la mejor arma contra la tristeza”
Reemplaza  todas las a del  String  anterior  por  una e,
adicionalmente  concatenar  a  esta  frase  una adicional
que ustedes quieran incluir por consola y las muestre
luego unidas.
* */
public class Text {
    public static void main(String[] args) {
        String text = "La sonrisa sera la mejor arma contra la tristeza ";
        String textReplace , finishText, inputText;

        textReplace = text.replace("a","e");
        System.out.println(textReplace);

        Scanner read = new Scanner(System.in);
        System.out.println("Insert Your New Text");
        inputText = read.nextLine();
        finishText = textReplace + inputText;

        System.out.println(finishText);
    }
}
