/*
Lee  un  número  por  teclado  que  pida  el  precio  de  un  producto  (puede  tener
decimales) y calcule el precio final con IVA. El IVA sera una constante que sera del
21%.
* */

import java.util.Scanner;

public class ProductPrice {
    public static void main(String[] args) {
        double IVA = 0.21;
        double price, total;

        Scanner read = new Scanner(System.in);

        System.out.println("Enter the Product Price");
        price = read.nextDouble();
        total = price * IVA + price;

        System.out.println("Price:       " + price +" $\n" +
                           "Product IVA:  " + price * IVA + " $\n" +
                           "Final Price: " + total + " $");
    }
}
