package SerieAndVideoGame.Class;

import SerieAndVideoGame.Interface.Entregable;

/*
Crearemos una clase Videojuego con las siguientes características:

 Sus atributos son titulo, horas estimadas, entregado, genero y compañia.
 Por  defecto,  las  horas  estimadas  serán  de  10  horas  y  entregado  false.  El  resto  de
  atributos serán valores por defecto según el tipo del atributo.

 Los constructores que se implementaran serán:
   Un constructor por defecto.
   Un constructor con el titulo y horas estimadas. El resto por defecto.
   Un constructor con todos los atributos, excepto de entregado.

 Los métodos que se implementara serán:
   Métodos get de todos los atributos, excepto de entregado.
   Sobrescribe los métodos toString
* */
public class VideoGames implements Entregable {

    private final String titulo ;
    private String genero ;
    private String compañia ;
    private final int horasEstimadas ;
    private boolean entregado ;

    private final int HORAS = 10 ;
    private final boolean ENTREGADO = false ;

    public VideoGames(){
        this.titulo = "" ;
        this.genero = "" ;
        this.compañia = "" ;
        this.entregado = ENTREGADO;
        this.horasEstimadas = HORAS;
    }

    public VideoGames(String titulo, int horas){
        this.titulo = titulo ;
        this.horasEstimadas = horas ;
    }

    public VideoGames(String titulo, String genero, String compañia,int horas){
        this.titulo = titulo ;
        this.genero = genero ;
        this.compañia = compañia ;
        this.horasEstimadas = horas;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getHorasEstimadas() {
        return horasEstimadas;
    }

    @Override
    public String toString() {
        return "titulo = '" + titulo + '\n' +
                "genero = '" + genero + '\n' +
                "compañia = '" + compañia + '\n' +
                "horasEstimadas = " + horasEstimadas + '\n' +
                "entregado = " + entregado;
    }


    @Override
    public void entregar() {
        this.entregado = true;
    }

    @Override
    public void devolver() {
        this.entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return entregado;
    }

    @Override
    public int compareTo(Object a) {
        return getHorasEstimadas();
    }
}
