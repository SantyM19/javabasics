package SerieAndVideoGame.Class;

import SerieAndVideoGame.Interface.Entregable;

/*
Crear una clase llamada Serie con las siguientes características:
 Sus atributos son título, numero de temporadas, entregado, género y creador.
 Por  defecto,  el  número  de  temporadas  es  de  3  temporadas  y  entregado false.
El resto de atributos serán valores por defecto según el tipo del atributo.
 Los constructores que se implementarán serán:
   Un constructor por defecto.
   Un constructor con el titulo y creador. El resto por defecto.
   Un constructor con todos los atributos, excepto de entregado.
 Los métodos que se implementara serán:
   Métodos get de todos los atributos, excepto de entregado.
   Sobrescribe los métodos toString.
* */
public class Serie implements Entregable {

    private final String titulo;
    private String genero;
    private final String creador;
    private int numeroTemporadas;
    private boolean entregado;

    private int NUMTEMPORADAS = 3;
    private boolean ENTREGADO = false;

    public Serie(){
        this.titulo = "";
        this.genero = "";
        this.creador = "";
        this.numeroTemporadas = NUMTEMPORADAS;
        this.entregado = ENTREGADO;
    }

    public Serie(String titulo , String creador){
        this.titulo = titulo;
        this.creador = creador;
    }

    public Serie(String titulo , String creador, String genero, int numeroTemporadas){
        this.titulo = titulo;
        this.genero = genero;
        this.creador = creador;
        this.numeroTemporadas = numeroTemporadas;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getGenero() {
        return genero;
    }

    public String getCreador() {
        return creador;
    }

    public int getNumeroTemporadas() {
        return numeroTemporadas;
    }

    @Override
    public String toString() {
        return "titulo = " + getTitulo() + '\n' +
                "genero = " + getGenero() + '\n' +
                "creador = " + getCreador() + '\n' +
                "numeroTemporadas = " + getNumeroTemporadas() + '\n' +
                "entregado = " + entregado;
    }

    @Override
    public void entregar() {
        this.entregado = true;
    }

    @Override
    public void devolver() {
        this.entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return entregado;
    }

    @Override
    public int compareTo(Object a) {
        return getNumeroTemporadas();
    }
}
