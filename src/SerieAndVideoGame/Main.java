package SerieAndVideoGame;

import SerieAndVideoGame.Class.Serie;
import SerieAndVideoGame.Class.VideoGames;

import java.util.ArrayList;

/*
Ahora crea una aplicación ejecutable
y realiza lo siguiente:
 Crea dos arrays, uno de Series
y otro de Videojuegos , de 5 posiciones cada uno.
 Crea un objeto en cada posición del array, con los
valores que desees, puedes usar distintos constructores.
 Entrega algunos Videojuegos y Series con el  método
entregar().

 Cuenta cuantos Series y Videojuegos
hay entregados. Al contarlos, devuélvelos.

 Por  último,  indica  el Videojuego
tiene  más  horas  estimadas  y  la  serie  con  mas
temporadas.  Muéstralos  en  pantalla  con  toda  su
información  (usa  el  método toString()).
* */
public class Main {

    public static int posicion = 0;
    public static int posicion2 = 0;
    public static int count = 0;
    public static int counto = 0;
    public static int numerosuperior = 0;

    public static void main(String[] args) {

        Serie serie1 = new Serie("Game Of Thrones","jumm","Drama Epico",8);
        Serie serie2 = new Serie("The Walking Dead","jumm", "Apocalipsis Zoombie", 10);
        Serie serie3 = new Serie("Vikings", "jumm", "Historico",7);
        Serie serie4 = new Serie("One Punch","jumm", "Anime",2);
        Serie serie5 = new Serie("Dragon BAll Z","jumm", "Anime",3);

        VideoGames vd1 = new VideoGames("League Of Leyends","RPGO","Riot",100);
        VideoGames vd2 = new VideoGames("Super Mario Bros", "aventura","Nintendo",4);
        VideoGames vd3 = new VideoGames("Rise Of Nations","estrategia","jumm",50);
        VideoGames vd4 = new VideoGames("Age Of Empires", "Estragtegia", "jumm",50);
        VideoGames vd5 = new VideoGames("Total War","Estrategia", "jumm",50);

        ArrayList<VideoGames> videoGames = new ArrayList<VideoGames>();
        ArrayList<Serie> series = new ArrayList<Serie>();

        videoGames.add(vd1);
        videoGames.add(vd2);
        videoGames.add(vd3);
        videoGames.add(vd4);
        videoGames.add(vd5);

        series.add(serie1);
        series.add(serie2);
        series.add(serie3);
        series.add(serie4);
        series.add(serie5);

        videoGames.get(0).entregar();
        videoGames.get(2).entregar();
        videoGames.get(4).entregar();

        series.get(1).entregar();
        series.get(3).entregar();

        videoGameEntrega(videoGames);

        System.out.println("---------------------------------");

        counto = 0;
        numerosuperior = 0;

        serieEntrega(series);

        System.out.println("---------------------------------");
        System.out.println("Cantidad Entregados:    " + count);
        System.out.println("=================================");

        System.out.println("VIDEO JUEGO CON MAYOR TIEMPO :");
        System.out.println(videoGames.get(posicion).toString());
        System.out.println("---------------------------------");
        System.out.println("SERIE CON MAYOR # TEMPORADAS :");
        System.out.println(series.get(posicion2).toString());

    }

    private static void serieEntrega(ArrayList<Serie> series) {
        for (Serie serie: series){
            if(serie.isEntregado()){
                count += 1;
                System.out.println("Serie Entregada:        " + serie.getTitulo());
            }
            if(serie.getNumeroTemporadas()>numerosuperior){
                posicion2 = counto;
                numerosuperior= serie.getNumeroTemporadas();
            }
            counto += 1;
        }
    }

    static void videoGameEntrega(ArrayList<VideoGames> videoGames) {
        for(VideoGames juegos: videoGames) {
            if (juegos.isEntregado()){
                count += 1;
                System.out.println("Video Game Entregado:   " + juegos.getTitulo());
            }

            if(juegos.getHorasEstimadas() > numerosuperior){
                posicion = counto;
                numerosuperior = juegos.getHorasEstimadas();
            }
            counto += 1;
        }
    }

}
