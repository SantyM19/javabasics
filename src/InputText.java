import java.util.Scanner;
/*
Realizar una aplicación de consola, que al ingresar una frase por teclado elimine
los espacios que esta contenga
* */

public class InputText {
    public static void main(String[] args) {
        String textReplace , inputText;

        Scanner read = new Scanner(System.in);
        System.out.println("Insert Your New Text");
        inputText = read.nextLine();
        textReplace = inputText.replace(" ","");

        System.out.println(textReplace);
    }
}
