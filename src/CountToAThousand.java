import java.util.Scanner;

public class CountToAThousand {
    public static void main(String[] args) {
        double number;

        Scanner read = new Scanner(System.in);

        System.out.println("Insert Your Number");
        number = Math.round(read.nextDouble());

        if(number > 1000.0){
            greaterThanThousand(number);
        }else{
            smallerThanThousand(number);
        }

        System.out.println(1000.0);
    }

    private static void smallerThanThousand(double number) {
        while(number < 998.0){
            number += 2.0;
            System.out.println(number);
        }
    }

    private static void greaterThanThousand(double number) {
        while(number >= 1002.0){
            number -= 2.0;
            if(1000.0 == number){
                break;
            }
            System.out.println(number);
        }
    }
}
