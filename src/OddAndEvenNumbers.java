/*
5. Muestra los números impares y pares del 1 al 100 (ambos incluidos). Usa un bucle
while.
6. Realiza el ejercicio anterior usando un ciclo for.
* */

public class OddAndEvenNumbers {
    public static void main(String[] args) {
        int x;
        String out;
        for(int i = 1; i < 101; i++){
            x = i % 2;
            out = (x == 0) ? "Number " + i + " Pair" : "Number " + i + " Odd";
            System.out.println(out);
        }
        /*
        int i = 1;
        while(i < 101){
            x = i % 2;
            out = (x == 0) ? "Number " + i + " Pair" : "Number " + i + " Odd";
            System.out.println(out);
            i++;
        }*/
    }
}
