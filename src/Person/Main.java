package Person;

import Person.Class.Person;

import javax.swing.*;
/*
Crea una clase ejecutable que haga lo siguiente:

*Pide por teclado el nombre, la edad, sexo, peso y altura.

*Crea 3 objetos de la clase anterior, el primer objeto obtendrá las
anteriores variables pedidas por teclado, el segundo objeto obtendrá
todos los anteriores menos el peso y la altura y el último por defecto,
para este último utiliza los métodos set para darle a los atributos un
valor.

*Para cada objeto, deberá comprobar si está en su peso ideal, tiene
sobrepeso o por debajo de su peso ideal con un mensaje.

*Indicar para cada objeto si es mayor de edad.

*Por último, mostrar la información de cada objeto.

* */

public class Main {
    public static void main(String[] args) {
        String name;
        int age;
        double weight, height;
        char gender;


        name = JOptionPane.showInputDialog("Type your name please");
        age = Integer.parseInt(JOptionPane.showInputDialog("How old are You"));
        Object gend = JOptionPane.showInputDialog(null,"Your Gender: ",
                "Gender", JOptionPane.QUESTION_MESSAGE, null,
                new Object[] { 'H', 'M' },"H");
        gender=gend.toString().charAt(0);
        weight = Double.parseDouble(JOptionPane.showInputDialog("Type Your Weight In Kg"));
        height = Double.parseDouble(JOptionPane.showInputDialog("Type Your Height In m"));

        JOptionPane.showMessageDialog(null, "Hello " + name + "\n" +
                "You are " + age + " Years Old\n" +
                "Gender " + gender + " " + weight + " " + height );

        Person persona1 = new Person(name,gender,age,height,weight);

        Person persona2 = new Person(name,gender,age);

        Person persona3 = new Person();

        persona3.setName(name);
        persona3.setGender(gender);
        persona3.setAge(age);
        persona3.setHeight(height);
        persona3.setWeight(weight);

        JOptionPane.showMessageDialog(null,persona1.getName() + " Persona 1, " + persona1.idealWeight());
        JOptionPane.showMessageDialog(null,persona2.getName() + " Persona 2, " + persona2.idealWeight());
        JOptionPane.showMessageDialog(null,persona3.getName() + " Persona 3, " + persona3.idealWeight());

        JOptionPane.showMessageDialog(null,persona1.getName() + " Persona 1, " + persona1.esMayor());
        JOptionPane.showMessageDialog(null,persona2.getName() + " Persona 2, " + persona2.esMayor());
        JOptionPane.showMessageDialog(null,persona3.getName() + " Persona 3, " + persona3.esMayor());

        JOptionPane.showMessageDialog(null, persona1.toString());
        JOptionPane.showMessageDialog(null, persona2.toString());
        JOptionPane.showMessageDialog(null, persona3.toString());

    }
}
