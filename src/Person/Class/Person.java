package Person.Class;

/*
Haz una clase llamada Persona que siga las siguientes condiciones:
Sus atributos son:
nombre, edad, DNI, sexo(H hombre, M mujer), peso y altura.

No queremos que se accedan directamente a ellos. Piensa que
modificador de acceso es el más adecuado, también su tipo. Si quieres añadir
algún atributo puedes hacerlo.

Por defecto, todos los atributos menos el DNI serán valores por defecto según
su tipo (0 números, cadena vacía para String, etc.). Sexo será hombre por
defecto, usa una constante para ello.

Se implantarán varios constructores:

 Un constructor por defecto.
 Un constructor con el nombre, edad y sexo, el resto por defecto.
 Un constructor con todos los atributos como parámetro.

Los métodos que se implementaran son:

* calcularIMC(): calculara si la persona está en su peso ideal (peso en kg/(altura^2 en m)),
si esta fórmula devuelve un valor menor que 20, la función devuelve un
-1, si devuelve un número entre 20 y 25 (incluidos), significa que está
por debajo de su peso ideal la función devuelve un 0  y si devuelve un valor mayor que 25 significa que
tiene sobrepeso, la función devuelve un 1. Te recomiendo que uses constantes para devolver estos valores.
esMayorDeEdad(): indica si es mayor de edad, devuelve un booleano.

* comprobarSexo(char sexo): comprueba que el sexo introducido es correcto. Si no es correcto, sera H.
No serávisible al exterior.

*toString(): devuelve toda la información del objeto.

*generaDNI(): genera un número aleatorio de 8 cifras, genera a partir de
este su número su letra correspondiente. Este método será invocado
cuando se construya el objeto. Puedes dividir el método para que te sea
más fácil. No será visible al exterior.

*Métodos set de cada parámetro, excepto de DNI.



* */

public class Person {

    private static char gender;
    private String name;
    private static Integer age;
    private final Integer id;
    private static double height;
    private static double weight;

    public Person(){
        name = "";
        age = 0;
        gender = 'H';
        weight = 0;
        height = 0;
        id = generaDNI();
    }

    public Person(String name,char gender, int age){
        this.name = name;
        this.gender = gender;
        this.age = age;
        id = generaDNI();
    }

    public Person(String name,char gender, int age,double height,double weight){
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.height = height;
        this.weight = weight;
        id = generaDNI();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public static char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public static double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public static double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Integer getId() {
        return id;
    }

    static int calcularIMC(){
        int idealWeight;
        double idealW;

        idealW = getWeight()/(Math.pow(getHeight(),2));

        if (idealW < 20) idealWeight =- 1;
        else if(idealW > 25)  idealWeight = 1;
        else idealWeight = 0;

        return idealWeight;
    }

    public String idealWeight(){
        String IMC;

        if(calcularIMC() == -1){
            IMC = "Estas Bajo de peso";
        }else if(calcularIMC() == 1){
            IMC = "Estas En Sobre Peso";
        }else{
            IMC = "Estas En tu peso Ideal";
        }

        return IMC;
    }

    static Boolean esMayorDeEdad(){
        return getAge() >= 18;
    }

    public String esMayor(){
        return (esMayorDeEdad()) ? "Mayor de Edad":"Menor de Edad";
    }

    /*
      comprobarSexo(char sexo): comprueba que el sexo introducido es correcto. Si no es correcto, sera H.
      No será visible al exterior.
    */

    static boolean comprobarSexo(char sexo){
        return sexo == getGender();
    }

    @Override
    public String toString() {
        String str;
        str =   "Name:    " + getName() + "\n" +
                "Age:     " + getAge() + "\n" +
                "ID:      " + getId() + "\n" +
                "Height:  " + getHeight() + " m\n" +
                "Weight:  " + getWeight() + "kg";
        return str;
    }

    public int generaDNI(){
        int id;
        id = (int) (Math.random() * 999_999_999) + 1;
        return id;
    }


}
