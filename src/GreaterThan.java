/*

1. Declara 2 variables numéricas (con el valor que desees), he indica cual es mayor
de  los  dos.  Si  son  iguales  indicarlo también.  Ve  cambiando  los  valores  para
comprobar que funciona

2. Al ejercicio anterior agregar entrada por consola de dos valores e indicar si son
mayores, menores o iguales.

* */

import java.util.Scanner;

public class GreaterThan {

    public static void main(String[] args) {
        double x, y;

        Scanner read = new Scanner(System.in);

        System.out.println("Enter First Number");
        x = read.nextDouble();

        System.out.println("Enter Second Number");
        y = read.nextDouble();

        System.out.println(Greater(x,y));

    }

    public static String Greater (double x,double y){
        String greater;
        if (x > y)
            greater = "x = " + x + " is greater than y = " + y;
        else if(x < y)
            greater = "y = " + y + " is greater than x = " + x;
        else
            greater = "y = " + y + " is equals to x = " + x;
        return greater;
    }
}
