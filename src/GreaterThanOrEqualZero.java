import java.util.Scanner;

/*
7. Lee un número por teclado y comprueba que este numero es mayor o igual que cero, si
no lo es lo volverá a pedir (do while), después muestra ese número por consola.
*/
public class GreaterThanOrEqualZero {
    public static void main(String[] args) {
        double number;

        Scanner read = new Scanner(System.in);

        do{
            System.out.println("Enter The Number");
            number = read.nextDouble();
        }while(number >= 0);
    }
}
