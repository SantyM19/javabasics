package HomeAppliance.Class;
/*
Crearemos una subclase llamada Lavadora
con las siguientes características:
 Su atributo es carga, además de los atributos heredados.
 Por defecto, la carga es de 5 kg. Usa una constante para ello.
 Los constructores que se
implementarán serán:
   Un constructor por defecto.
   Un constructor con el precio y peso. El resto por defecto.
   Un constructor con la carga y el resto de atributos heredados. Recuerda que
    debes llamar al constructor de la clase padre.
 Los métodos que se implementara serán:
   Método get de carga.
   precioFinal():, si tiene una carga mayor de 30 kg, aumentara el precio 50 €, sino
    es así no se incrementara el precio. Llama al método padre y añade el código
    necesario. Recuerda que las condiciones que hemos visto en la clase Electrodomestico
    también deben afectar al precio.
* */
public class Lavadora extends Electrodomestico {
    protected double carga;
    private final double CARGA = 5.0;

    public Lavadora (){
        carga=CARGA;
    }

    public Lavadora (double price, double weight){
        super(price, weight);
        carga=CARGA;
    }

    public Lavadora (double price, double weight, String color, char ec, double carga){
        super (price,weight,color,ec);
        this.carga=carga;
    }

    public double getCarga() {
        return carga;
    }

    @Override
    public double precioFinal() {
        double precioFinal = super.precioFinal() + getPrice();

        return (getCarga()>30.0)? precioFinal += 50.0: precioFinal;

    }
}
