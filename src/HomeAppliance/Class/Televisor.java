package HomeAppliance.Class;
/*
 Sus atributos son resolución (en pulgadas) y sintonizador TDT (booleano),
  además de los atributos heredados.
 Por defecto, la resolución será de 20 pulgadas y el sintonizador será false.
 Los constructores que se implementarán serán:
   Un constructor por defecto.
   Un constructor con el precio y peso. El resto por defecto.
   Un constructor con la resolución, sintonizador TDT y el resto de atributos
    heredados. Recuerda que debes llamar al constructor de la clase padre.
 Los métodos que se implementara serán:
   Método get de resolución y sintonizador TDT.
   precioFinal(): si tiene una resolución mayor de 40 pulgadas, se incrementara el
    precio un 30% y si tiene un sintonizador TDT incorporado, aumentara 50€.
    Recuerda que las condiciones que hemos visto en la clase Electrodomestico
    también deben afectar al precio.
* */
public class Televisor extends Electrodomestico{
    protected double resolucion;
    protected boolean tdt;
    private final double RESOLUCION = 5.0;
    private final boolean TDT = true;

    public Televisor (){
        resolucion = RESOLUCION;
        tdt = TDT;
    }

    public Televisor (double price, double weight){
        super(price, weight);
        resolucion = RESOLUCION;
        tdt = TDT;
    }

    public Televisor (double price, double weight, String color, char ec, double resolucion, boolean tdt){
        super (price,weight,color,ec);
        this.resolucion = resolucion;
        this.tdt = tdt;
    }

    public double getResolucion() {
        return resolucion;
    }

    public boolean getTdt() {
        return tdt;
    }

    @Override
    public double precioFinal() {
        double precioFinal = super.precioFinal() + getPrice();

        if(getResolucion() > 40.0){
            precioFinal += precioFinal * .3;
        }

        if(getTdt()){
            precioFinal += 50.0;
        }

        return precioFinal;

    }
}
