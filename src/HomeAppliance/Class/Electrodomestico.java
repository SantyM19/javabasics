package HomeAppliance.Class;

import java.util.HashMap;

/*
 Sus atributos son precio base, color, consumo energético (letras entre A y F) y peso.
  Indican que se podrán heredar.
 Por defecto, el color será blanco, el consumo energético será F, el
  precioBase es de 100 € y el peso de 5 kg. Usa constantes para ello.
 Los colores disponibles son blancos, negro, rojo, azul y gris. No importa si el
  nombre esta en mayúsculas o en minúsculas.
 Los constructores que se implementarán serán:
    Un constructor por defecto.
    Un constructor con el precio y peso. El resto por defecto.
    Un constructor con todos los atributos.
 Los métodos que implementara serán:
    Métodos get de todos los atributos.
    comprobarConsumoEnergetico(char letra):
     comprueba que la letra es correcta, sino es correcta usara la letra por defecto.
     Se invocará al crear el objeto y no será visible.
    comprobarColor(String color):
     comprueba que el color es correcto, sino lo es usa el color por defecto.
     Se invocará al crear el objeto y no será visible.
    precioFinal(): según el consumo energético, aumentara su precio, y según su
     tamaño, también. Esta es la lista de precios:
* */
public class Electrodomestico {
    protected double price;
    protected String color;
    protected char energyConsumption;
    protected double weight;

    private final String COLOR = "Blanco";
    private final char EC = 'F';

    public Electrodomestico(){
        //Euros
        price = 100.0;
        color = COLOR;
        energyConsumption = EC;
        //Kg
        weight = 5.0;
    }

    public Electrodomestico(double price, double weight){
        this.price = price;
        this.weight = weight;
        color = COLOR;
        energyConsumption = EC;
    }

    public Electrodomestico(double price, double weight, String color, char ec){
        this.price = price;
        this.weight = weight;
        this.color = color;
        this.energyConsumption = ec;
    }

    public double getPrice() {
        return price;
    }

    public String getColor() {
        return color;
    }

    public char getEnergyConsumption() {
        return energyConsumption;
    }

    public double getWeight() {
        return weight;
    }

    public boolean comprobarConsumoEnergetico(char letra){
        return getEnergyConsumption()==letra;
    }

    public  boolean comprobarColor(String color){
        return getColor().equals(color);
    }

    public double precioFinal(){
        double totalPrice;

        totalPrice = getPriceForEnerGyConsumption(getEnergyConsumption());
        totalPrice += getPriceForWeight(getWeight());

        return totalPrice;
    }

    private double getPriceForWeight(double weight) {
        double valor = 0;

        if (getWeight() > 79)
            valor = 100;
        if(getWeight() <= 79.0)
            valor = 80;
        if(getWeight() <= 49.0)
            valor = 50;
        if (getWeight() <= 19.0)
            valor =  10;

        return valor;
    }

    private double getPriceForEnerGyConsumption(char energyConsumption) {

        HashMap<Character, Double> energyConsuption = new HashMap<Character, Double>();

        energyConsuption.put('A', 100.0);
        energyConsuption.put('B', 80.0);
        energyConsuption.put('C', 60.0);
        energyConsuption.put('D', 50.0);
        energyConsuption.put('E', 30.0);
        energyConsuption.put('F', 10.0);

        return energyConsuption.get(energyConsumption);
    }


}
