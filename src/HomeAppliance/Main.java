package HomeAppliance;

/*
Ahora crea una clase ejecutable que realice lo siguiente:
 Crea un array de Electrodomesticos de 10 posiciones.
 Asigna a cada posición un objeto de las clases anteriores con los valores que desees.
 Ahora, recorre este array y ejecuta el método precioFinal().
 Deberás mostrar el precio de cada clase, es decir, el precio de todas las
  televisiones por un lado, el de las lavadoras por otro y la suma de los
  Electrodomesticos (puedes crear objetos Electrodomestico, pero recuerda que
  Television y Lavadora también son electrodomésticos). Recuerda el uso operador
  instanceof.

Por ejemplo, si tenemos un Electrodomestico con un precio final de 300, una lavadora de
200 y una televisión de 500, el resultado final sera de 1000 (300+200+500) para
electrodomésticos, 200 para lavadora y 500 para televisión.
* */

import HomeAppliance.Class.Electrodomestico;
import HomeAppliance.Class.Lavadora;
import HomeAppliance.Class.Televisor;


import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        double lavadorasPrecio = 0;
        double tvsPrecio = 0;
        double electrodomesticosPrecio = 0;

        Lavadora Lavadora1 = new Lavadora(400, 80.5, "blanco", 'B', 40);
        Lavadora Lavadora2 = new Lavadora(600, 60.5, "gris", 'A', 50);
        Lavadora Lavadora3 = new Lavadora(200, 40.5);
        Lavadora Lavadora4 = new Lavadora();
        Lavadora Lavadora5 = new Lavadora();

        Televisor tv1 = new Televisor(500, 32, "negro", 'A', 50, true);
        Televisor tv2 = new Televisor(600, 32, "negro", 'B', 50, false);
        Televisor tv3 = new Televisor(500, 40);
        Televisor tv4 = new Televisor();
        Televisor tv5 = new Televisor();

        ArrayList<Electrodomestico> electrodomesticos = new ArrayList<Electrodomestico>();
        electrodomesticos.add(Lavadora1);
        electrodomesticos.add(Lavadora2);
        electrodomesticos.add(Lavadora3);
        electrodomesticos.add(Lavadora4);
        electrodomesticos.add(Lavadora5);
        electrodomesticos.add(tv1);
        electrodomesticos.add(tv2);
        electrodomesticos.add(tv3);
        electrodomesticos.add(tv4);
        electrodomesticos.add(tv5);

        tvsPrecio = tvsPrecioTotal(electrodomesticos);

        System.out.println("------------------------");
        System.out.println("Total TVs:       " + tvsPrecio);
        System.out.println("========================");

        lavadorasPrecio = lavadorasPrecioTotal(electrodomesticos);

        System.out.println("------------------------");
        System.out.println("Total Lavadoras: " + lavadorasPrecio);
        System.out.println("========================");

        electrodomesticosPrecio = tvsPrecio + lavadorasPrecio;
        System.out.println("Gran Total:      " + electrodomesticosPrecio);
        System.out.println("========================");

    }

    private static double lavadorasPrecioTotal(ArrayList<Electrodomestico> electrodomesticos) {
        double lavadorasPrecio = 0;
        for(Electrodomestico dispositivo: electrodomesticos) {
            if(dispositivo instanceof Lavadora){
                System.out.println("Lavadora Price:  " + dispositivo.precioFinal());
                lavadorasPrecio += dispositivo.precioFinal();
            }
        }
        return lavadorasPrecio;
    }

    private static double tvsPrecioTotal(ArrayList<Electrodomestico> electrodomesticos){
        double tvsPrecio = 0;
        for(Electrodomestico dispositivo: electrodomesticos) {
            if(dispositivo instanceof Televisor){
                System.out.println("TV Price:        " + dispositivo.precioFinal());
                tvsPrecio += dispositivo.precioFinal();
            }
        }
        return tvsPrecio;
    }

}
